public class AlgoMatdisSemester2{
    public static void main(String[] args) {
        //System.out.println(gcd(987, 989));
        //printArray(toOther(5238, 8), index(5238, 8));
        //modHighPower(43, 5, 213);
        trialDivision(5361);
    }

    // Algoritma gcd euclidean.
    public static int gcd(int x, int y){
        // x > y
        System.out.println(x + " = " + y + "." + x/y + " + " + (x % y));
        if(x % y == 0)
            return y;
        return gcd(y, x % y);
    }

    /* Tabel pembagian modular dengan pangkat tinggi.
       Masih perlu banyak perbaikan. */ 
    public static void modHighPower(int basis, int pangkat, int mod){
        int index = index(pangkat, 2);
        int[] converted = toOther(pangkat, 2);
        int x = 0, calcPower = 0, power = 0, saverCalcPower = 0, saverPower = 0, saverX = 0;
        System.out.println("i   ai               x               calc power               power");
        for(int i = 0; i < index; i++){
            if(i == 0 && converted[i] == 0){
                x = 1;
                calcPower = (int) Math.pow(basis, 2) % mod; 
                power = calcPower;
                System.out.println(i + "   " + converted[i] + "                " + 1 + "               " + (basis + "^" + 2 + " mod " + mod + " = " + calcPower) + "          " + power);
            } else if(i == 0 && converted[i] == 1){
                x = basis % mod;
                calcPower = (int) Math.pow(basis, 2) % mod;
                power = calcPower;
                System.out.println(i + "   " + converted[i] + "        " + (basis + " mod " + mod + " = " + (basis % mod)) + "          " + (basis + "^" + 2 + " mod " + mod + " = " + calcPower) + "          " + power);
            } else{
                if(converted[i] == 1){
                    saverX = x;
                    x = (x * calcPower) % mod; 
                    saverPower = power;
                    saverCalcPower = calcPower;
                    calcPower = (int) Math.pow(power, 2) % mod;
                    power = calcPower;
                    System.out.println(i + "   " + converted[i] + "        " + (saverX + "." + saverCalcPower + " mod " + mod + " = "  + x) + "          " + (saverPower + "^" + 2 + " mod " + mod + " = " + calcPower) + "          " + power);
                } else{
                    saverPower = power;
                    saverCalcPower = calcPower;
                    calcPower = (int) Math.pow(power, 2) % mod;
                    power = calcPower;
                    System.out.println(i + "   " + converted[i] + "               " + x + "               " + (saverPower + "^" + 2 + " mod " + mod + " = " + calcPower) + "          " + power);
                }
            }
        }
    }

    // Konversi angka ke basis yang bukan 16
    public static int[] toOther(int angka, int tujuanBasis){

        // Pembatasan array dengan asumsi konversi tidak akan melebihi batas yang ditentukan. 
        int[] converted = new int[20];
        int index = 0;

        // Tampilan yang akan muncul untuk mempermudah pembacaan.
        System.out.println("Konvers: " + " desimal " + angka + " ke basis " + tujuanBasis);
        while (angka > 0){
            converted[index++] = angka % tujuanBasis;
            System.out.println(angka + " mod " + tujuanBasis + " = " + (angka % tujuanBasis));
            angka /= tujuanBasis;
            System.out.println(angka + " div " + tujuanBasis + " = " + (angka / tujuanBasis));
            System.out.println("");
        } return converted;
    }

    public static int index(int angka, int tujuanBasis){
        int index = 0;
        while (angka > 0){
            index++;
            angka /= 2;
        } return index;
    }

    public static void printArray(int[] converted, int index){
        System.out.print("Hasil = ");
        for(int i = index - 1 ; i >= 0; i--){
            System.out.print(converted[i]);
        }
        System.out.println("");
    }

    // Algortma trial division.
    public static void trialDivision(int num){

        // Penentuan batas.
        int batas = (int) Math.sqrt(num);

        // Iterasi pembagian.
        // Ketika suatu bilangan habis membagi bilangan maka bilangan tidak prima.
        for(int i=2; i < batas; i++){
            int mod = num % i;
            if(mod == 0){
                System.out.println(num + " % " + i + " = " + mod + ", karena habis dibagi " + i + " maka tidak prima");
                break;
            }
            else    
                System.out.println(num + " % " + i + " = " + mod);
        }
    }
}