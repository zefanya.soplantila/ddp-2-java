public class Inst{
    public static void main(String[] args) {
        java.util.Scanner in = new java.util.Scanner(System.in);

        //System.out.println(hexCode(in.nextLine()));
        while(true){
            System.out.print("HEX OR BIN (\"Exit\" for exit): ");
            String formatInput = in.nextLine();

            if(formatInput.equals("Exit"))
                break;
            else if(formatInput.equals("BIN")){
                System.out.print("Enter code: ");
                String code = in.nextLine();
                cekFormat(code);
                System.out.print("Hex Code: " + binToHex(code) + "\n");
            } else{
                System.out.print("Enter code: ");
                String code = in.nextLine();
                cekFormat(hexCode(code));
                System.out.print("Bin code: " + hexCode(code) + "\n");
            }
        }

        in.close();
    }

    public static String getRegister(int regNum){
        String[] regList = {"$zero", "$at", "$v0", "$v1", "$a0", "$a1", "$a2", "$a3",
                            "$t0", "$t1", "$t2", "$t3", "$t4", "$t5", "$t6", "$t7",
                            "$s0", "$s1", "$s2", "$s3", "$s4", "$s5", "$s6", "$s7",
                            "$t8", "$t9"};
        return regList[regNum];
    }

    public static void cekFormat(String code){
        if(binToDec(code.substring(0, 6)) == 0){
            System.out.println("R Format: ");
            System.out.println(RFormat(code));
        }
        else{
            if(IFormat(code) != null){
                System.out.println("I Format: ");
                System.out.println(IFormat(code));
            }
            else{
                System.out.println("J Format: ");
                System.out.println(JFormat(code));
            }
        }
    }

    public static String JFormat(String binary){
        String inst;
        String address = binary.substring(6, 31);
        switch(binToDec(binary.substring(0, 6))){
            case 2 : inst = "j"; break;
            case 3 : inst = "jal"; break;
            default : return null;
        }
        return inst + address; 
    }

    public static String IFormat(String binary){
        String inst;
        
        switch(binToDec(binary.substring(0, 6))){
            case 4  : inst = "beq"; break;
            case 5  : inst = "bne"; break;
            case 8  : inst = "addi"; break;
            case 9  : inst = "addiu"; break;
            case 12 : inst = "andi"; break;
            case 36 : inst = "lbu"; break;
            case 37 : inst = "lhu"; break;
            case 48 : inst = "ll"; break;
            case 15 : inst = "lui"; break;
            case 35 : inst = "lw"; break;
            case 13 : inst = "ori"; break;
            case 10 : inst = "slti"; break;
            case 11 : inst = "sltiu"; break;
            case 40 : inst = "sb"; break;
            case 41 : inst = "sh"; break;
            case 43 : inst = "sw"; break;
            default : return null;
        }
        String source = getRegister(binToDec(binary.substring(6, 11))) + "[$" + binToDec(binary.substring(6, 11)) + "]";;
        String target = getRegister(binToDec(binary.substring(11, 16))) + "[$" + binToDec(binary.substring(11, 16)) + "]";
        String immediate = String.valueOf(binToDec(binary.substring(16)));

        if(!(inst.equals("lw")) && !(inst.equals("sw")) && !(inst.equals("lb")) && !(inst.equals("sb")) && !(inst.equals("lbu")) && !(inst.equals("lhu")) && !(inst.equals("lui"))){
            return inst + " " + source + ", " + target + ", " + immediate + "\n"
            + "rs: " + source + "\n"
            + "rt: " + target + "\n"
            + "immediate: " + immediate + "\n";
        } else{
            return inst + " " + target + ", " + immediate + "(" + source + ")" + "\n"
            + "rs: " + source + "\n"
            + "rt: " + target + "\n"
            + "immediate: " + immediate + "\n";
        }
    }

    public static String RFormat(String binary){
        String inst;
        switch(binToDec(binary.substring(26, 32))){
            case 0  : inst = "sll"; break;
            case 8  : inst = "jr"; break;
            case 32 : inst =  "add"; break;
            case 2  : inst = "srl"; break;
            case 33 : inst =  "addu"; break;
            case 34 : inst =  "sub"; break;
            case 35 : inst =  "subu"; break;
            case 36 : inst =  "and"; break;
            case 37 : inst =  "or"; break;
            case 39 : inst =  "nor"; break;
            case 42 : inst =  "slt"; break;
            default : return null;
        }
        if(!inst.equals("jr")){
            String source = getRegister(binToDec(binary.substring(6, 11))) + "[$" + binToDec(binary.substring(6, 11)) + "]";
            String target = getRegister(binToDec(binary.substring(11, 16))) + "[$" + binToDec(binary.substring(11, 16)) + "]";
            String destination = getRegister(binToDec(binary.substring(16, 21))) + "[$" + binToDec(binary.substring(16, 21)) + "]";

            return inst + " " + destination + ", " + source + ", " + target + "\n"
            + "rs: " + source + "\n"
            + "rt: " + target + "\n"
            + "rd: " + destination + "\n";
        } else{
            return inst + " $ra ($31)";  
        }
    }

    public static int binToDec(String binary){
        int total = 0;
        for(int i = 0; i < binary.length(); i++){
            total += Math.pow(2, (binary.length() - 1) - i) * Character.getNumericValue(binary.charAt(i));
        } return total;
    }

    public static String decToBin(int dec){
        String bin = "";
        while(dec != 0){
            bin += String.valueOf(dec % 2);
            dec /= 2;
        } return bin;
    }

    public static String hexCode(String hex){
        String code = "";
        for(int i = 0; i < hex.length(); i++){
            if(Character.isDigit(hex.charAt(i)))
                switch(hex.charAt(i)){
                    case '0' : code += "0000"; break;
                    case '1' : code += "0001"; break;
                    case '2' : code += "0010"; break;
                    case '3' : code += "0011"; break;
                    case '4' : code += "0100"; break;
                    case '5' : code += "0101"; break;
                    case '6' : code += "0110"; break;
                    case '7' : code += "0111"; break;
                    case '8' : code += "1000"; break;
                    case '9' : code += "1001"; break;
                }
            else
                switch(hex.charAt(i)){
                    case 'A' : code += "1010"; break;
                    case 'B' : code += "1011"; break;
                    case 'C' : code += "1100"; break;
                    case 'D' : code += "1101"; break;
                    case 'E' : code += "1110"; break;
                    case 'F' : code += "1111"; break;
                }
        } return code;
    }

    public static String binToHex(String code){
        String result = "0x";
        for(int i = 0; i < 29; i += 4){
            String part = code.substring(i, i + 4);
            if(binToDec(part) < 10)
                result += binToDec(part);
            else
                switch(binToDec(part)){
                    case 10 : result += 'A'; break;
                    case 11 : result += 'B'; break;
                    case 12 : result += 'C'; break;
                    case 13 : result += 'D'; break;
                    case 14 : result += 'E'; break;
                    case 15 : result += 'F'; break;
                }
        } return result;
    }
}