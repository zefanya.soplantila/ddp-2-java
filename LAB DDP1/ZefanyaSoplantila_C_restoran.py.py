jumlah = int(input("Masukkan Jumlah Donat DUARRR : "))
dictorasa = {}
dictonama = {}
dictocampur = {}
lst = []
harga_total = 0
nama_donut = []

'''Beberapa perlengkapan diatas akan digunakan dalam proses berjalannya program'''

for a in range(jumlah): #Menampilkan user prompt sebanyak jumlah integer yang dimasukkan di awal
    masukkan = input(f"Data {a + 1} : ")
    lst.append(masukkan.split()) #Membuat sebuah list dari input yang diberikan oleh user yang kemudian akan dikelompokkan
    
    dictorasa[lst[a][2]] = int(lst[a][1])
    dictonama[lst[a][0]] = int(lst[a][1])
    dictocampur[lst[a][2]] = lst[a][0]

'''3 dictionary diatas digunakan untuk storing nama, rasa dan harganya. Dibuat sebanyak 3 agar
pada waktu proses perjalanan program dapat diambil suatu nilai dari salah satu dictionary diatas
'''

pembeli = int(input("Masukkan Jumlah Pembeli : ")) #User prompt jumlah pembeli
for a in range(pembeli): #Iterasi pesanan sesuai dengan banyaknya pembeli
    pesanan = input(f"Pembeli {a + 1} : ")
    splitter = pesanan.split() #Membuat list dari input user yang kemudian akan dibedakan menurut keperluannya
    if splitter[0] == "BELI_RASA": #Proses akan dilakukan jika pada string terdapat "BELI_RASA"
        if splitter[1] in dictorasa:
            harga_total += dictorasa[splitter[1]] #Storing harga dari semua yang dibeli
            nama_donut.append(dictocampur[splitter[1]]) #Menambahkan jenis donut yang dibeli
            print(f'{dictocampur[splitter[1]]} terjual dengan harga {dictorasa[splitter[1]]}')
        else:
            print(f"Tidak ada donat DUARR!! dengan rasa {splitter[1]}")
    elif splitter[0] == "BELI_NAMA": #Proses akan dilakukan jika pada string terdapat "BELI_NAMA"
        if splitter[1] in dictonama:
            harga_total += dictonama[splitter[1]] #Storing harga dari semua yang dibeli
            nama_donut.append(splitter[1]) #Menambahkan jenis donut yang dibeli
            print(f'{splitter[1]} terjual dengan harga {dictonama[splitter[1]]}')
        else:
            print(f"Tidak ada donut DUARR!! dengan nama {splitter[1]}")

print("Total Penjualan :", harga_total)
print("Donut terjual :")
for a in set(nama_donut):
    print(a)
            
      