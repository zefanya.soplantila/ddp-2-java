from tkinter import *
from tkinter.filedialog import asksaveasfilename, askopenfilename


class Application(Frame):
    def __init__(self, master=None): #Inisiasi attribut
        super().__init__(master)
        self.master = master
        self.initUI()
        self.create_buttons()
        self.create_editor()

    def initUI(self):
        # mengatur judul dan ukuran dari main window,
        # lalu buat sebuah Frame sebagai anchor dari seluruh button
        self.master.title('Pacil Editor')
        self.master.geometry('500x500')
        self.frame = Frame(bg = 'mintcream')
        self.frame.pack()

    def create_buttons(self):
        # Terdapat 3 button dengan fungsi masing-masing yang dibutuhkan
        self.button2 = Button(self.frame, text = 'Quit', command = exit, bg = 'LightCyan2')
        self.button2.pack(side = BOTTOM)
        self.button1 = Button(self.frame, text = 'Save File', command = self.save_file_event, bg = 'LightCyan2')
        self.button1.pack(side = BOTTOM)
        self.button0 = Button(self.frame, text = 'Open File', command = self.load_file_event, bg = 'LightCyan2')
        self.button0.pack(side = BOTTOM)
        
        

    def create_editor(self):
        # Pembuatan text box dengan font yang sudah diatur sedemikian rupa
        editor = Text(self.frame, relief = FLAT, font = 'CourierNew 10' )
        editor.pack()
        self.edit = editor
        
        
    def load_file_event(self): #Method untuk load file yang berhubungan dengan button open file
        self.load_file()

    def load_file(self):
        # Penggunaan fungsi bawaan askopenfilename dengan tidak ada pembatasan tipe file
        file_name = askopenfilename(
            filetypes=[("All files", "*")]
        )
        if not file_name:  # Jika pengguna membatalkan dialog, langsung return
            return
        text_file = open(file_name, 'r', encoding="utf-8")
        result = text_file.read()
        
        # menampilkan result di textbox
        self.set_text(result)

    def save_file_event(self): #Implementasi method save file
        self.save_file()

    def save_file(self):
        #Penggunaan method asksaveasfilename untuk save file
        file_name = asksaveasfilename(
            filetypes=[("All files", "*")]
        )
        text = self.get_text()
        if not file_name:  # Jika pengguna membatalkan dialog, langsung return
            return
        #Membuka file atau membuat file lalu kemudian save tulisan di file tersebut 
        with open(file_name, 'w') as pembuka:
            pembuka.write(text)



    def set_text(self, text=''):
        self.edit.delete('1.0', END)
        self.edit.insert('1.0', text)
        self.edit.mark_set(INSERT, '1.0')
        self.edit.focus()

    def get_text(self):
        return self.edit.get('1.0', END+'-1c')


if __name__ == "__main__":
    root = Tk()
    app = Application(master=root)
    app.mainloop()